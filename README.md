

```
#!
git clone https://udaw@bitbucket.org/udaw/dcodtesttask.git
cd dcodtesttask
virtualenv --no-site-packages .env
source .env/bin/activate 
pip install -r requirements.txt
python manage.py migrate
```

One way is to call management command which downloads file and fills DB accordingly:
```
#!
python manage.py loaddcoddata --url http://www.test.d-cod.com/2CuNPefD.json
python manage.py runserver
```

Another way is to create superuser, upload file via Django admin and run as follows: 
```
#!
python manage.py loaddcoddata --file 2CuNPefD.json
python manage.py runserver
```