"""Registring models."""
from django.contrib import admin

from models import Area, City, Upload

admin.site.register(Area)
admin.site.register(City)
admin.site.register(Upload)
