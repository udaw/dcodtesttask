"""Models to store areas and cities with data."""
from __future__ import unicode_literals
from django.db import models
import os


class AreaManager(models.Manager):
    """Manager in order to proceed with natural keys."""

    def get_by_natural_key(self, name):
        """In order to support natural keys."""
        return self.get(name=name)


class Area(models.Model):
    """Area model."""

    name = models.CharField(max_length=100, unique=True)
    objects = AreaManager()

    def natural_key(self):
        """In order to support natural keys."""
        return (self.name,)

    def __unicode__(self):
        return self.name


class City(models.Model):
    """City model."""

    name = models.CharField(max_length=100, unique=True)
    val = models.FloatField()
    area = models.ForeignKey(Area)

    def __unicode__(self):
        return self.name


class Upload(models.Model):
    """Simple model to store files."""

    docfile = models.FileField(upload_to='documents')

    def delete(self, *args, **kwargs):
        """Delete old file on delete."""
        if os.path.isfile(self.docfile.path):
            os.remove(self.docfile.path)

        super(Upload, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        """Delete old file on update."""
        try:
            this = Upload.objects.get(id=self.id)
            this.docfile.delete(save=False)
        except:
            pass
        super(Upload, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.docfile.name
