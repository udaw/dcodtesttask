"""Tests."""
from django.test import TestCase, Client
from django.core.urlresolvers import reverse


class PageTest(TestCase):
    """Simple view test."""

    def setUp(self):
        """Test needs a client."""
        self.client = Client()

    def test_chart_page(self):
        """Check that responce is 200 ok, template used."""
        url = reverse('charts:chart')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'charts/chart.html')
