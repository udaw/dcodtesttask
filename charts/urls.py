"""Urls."""
from django.conf.urls import url
from django.http import HttpResponseRedirect
from views import ChartView

urlpatterns = [
    url(r'^$', lambda r: HttpResponseRedirect('chart/')),
    url(r'^chart/$', ChartView.as_view(), name='chart'),
]
