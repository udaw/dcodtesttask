$( document ).ready(function() {
$(function () {

    $.getJSON('http://127.0.0.1:8000/chart/', function (data) {

        var area_chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'Some Area data from DCOD json file.'
            },
            subtitle: {
                text: 'Source: <a href="http://www.test.d-cod.com/2CuNPefD.json">DCOD</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Some data'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Data: <b>{point.y:.1f} </b>'
            },
            series: [{
                name: 'Population',
                data: [
                    ['Select Area', 100]
                ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });

        var myOptions = data['Areas'];
        var _select = $('<select>');
        $.each(myOptions, function(val, text) {
            _select.append(
                    $('<option></option>').val(text).html(text)
                );
        });
        $('#dropdownlist').append(_select.html());

        $("#dropdownlist").change(function() {
            var type = this.value;
            upd_data = [];
            data['Cities'].forEach(function(obj) {
                if (obj['area'] == type) {
                    upd_data.push([obj['city'], obj['val']])
                }
            });

            if(type !== '0') {
                area_chart.update({
                    series: {
                        data: upd_data,
                    }
                })
            }
            else {
                area_chart.update({
                    series: {
                        data: [['Select Area', 100]],
                    }
                })
            }

        });

    });

});
});

