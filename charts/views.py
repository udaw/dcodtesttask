"""View called to provide all areas and cities data to chart."""
from django.views.generic import TemplateView
from django.http import HttpResponse
from models import Area, City
import json


class ChartView(TemplateView):
    """View called to proceed with chart."""

    template_name = "charts/chart.html"

    def data_for_chart(self):
        """Prepare data fo Ajax response."""
        data = dict()
        areas = Area.objects.all()
        cities = City.objects.all()
        data["Areas"] = [area.name for area in areas]
        data["Cities"] = [{
            "city": city.name,
            "area": city.area.name,
            "val": city.val
            } for city in cities]
        return data

    def get(self, request, *args, **kwargs):
        """Overriding get in order to proceed with Ajax."""
        if request.is_ajax():
            data = json.dumps(self.data_for_chart())
            kwargs['content_type'] = 'application/json'
            return HttpResponse(data, **kwargs)
        return super(ChartView, self).get(request, *args, **kwargs)
