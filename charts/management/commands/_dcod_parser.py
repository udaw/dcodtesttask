"""Going to parse DCOD json file to fill db."""
import json
from charts.models import Area, City
from django.db import IntegrityError


def dcod_parser(dcod_json):
    """Parse json into areas and cities and fill db."""
    areas = set()
    cities = list()

    data = json.loads(dcod_json)
    for d in data['data']:
        city = []
        for k, v in d.iteritems():
            if k == data['structure'][0]:
                areas.add(v)
            city.append(v)
        cities.append(city)

    fill_db(areas, cities)


def fill_db(areas, cities):
    """Create tables rows. Duplicates prohibited."""
    for area in areas:
        try:
            Area.objects.create(name=area)
        except IntegrityError as e:
            print str(e)

    for city in cities:
        try:
            City.objects.create(
                val=float(city[0]),
                area=Area.objects.get(name=city[1]),
                name=city[2]
                )
        except IntegrityError as e:
            print str(e)
