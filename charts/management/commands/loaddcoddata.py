"""Custom command able to fill database with data parsed from source file."""
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from charts.models import Upload
from _dcod_parser import dcod_parser
import requests


class Command(BaseCommand):
    """Command is able to proceed with json only."""

    def add_arguments(self, parser):
        """Available to download file or proceed via stored files."""
        parser.add_argument('-f', '--file', type=str, metavar=('FILE'))
        parser.add_argument('-u', '--url', type=str, metavar=('URL'))

    def handle(self, *args, **options):
        """Handling both options."""
        if options['url']:
            url = options['url']
            try:
                response = requests.get(url)
                if not response.status_code // 100 == 2:
                    raise CommandError(
                        "Error: Unexpected response {}".format(response))
                dcod_parser(response.text)
            except requests.exceptions.RequestException as e:
                raise CommandError("Error: {}".format(e))

            self.stdout.write(
                self.style.SUCCESS(
                    'Successfully loaded from {url}'.format(url=url)))

        if options['file']:
            name = options['file']
            try:
                dcod_file = Upload.objects.get(docfile='documents/'+name)
                dcod_parser(dcod_file.docfile.read())
            except(ObjectDoesNotExist, MultipleObjectsReturned) as e:
                raise CommandError(
                    'File {name} Error: {err}'.format(name=name, err=e))

            self.stdout.write(
                self.style.SUCCESS(
                    'Successfully loaded "{name}"'.format(name=name)))
