# -*- coding: utf-8 -*-
"""Going to parse DCOD json file and prepare fixtures.

In order to proceed with migrations.
"""
import json
import os


AREAS_FIXTURE = u"""
{{
    "model": "charts.area",
    "pk": {pk},
    "fields": {{
        "name": "{name}"
    }}
}},"""
CITIES_FIXTURE = u"""
{{
    "model": "charts.city",
    "pk": {pk},
    "fields": {{
        "name": "{name}",
        "val": {val},
        "area": [
            "{area_name}"
        ]
    }}
}},"""


def dcod_parser(dcod_json):
    """Parse json into areas set and cities dict."""
    areas = set()
    cities = list()
    with open(dcod_json) as data_file:
        data = json.load(data_file)
    for data in data['data']:
        city = []
        for k, v in data.iteritems():
            if k == u'Область':
                areas.add(v)
            city.append(v)
        cities.append(city)

    return list(areas), cities


def areas_to_fixtures(areas):
    """Add area fixtures file."""
    path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), '..', 'fixtures/initial_areas.json'))
    pk = 1
    fixture = ""
    with open(path, 'w') as data_file:
        data_file.write("[")
        for area in areas:
            fixture += AREAS_FIXTURE.format(pk=pk, name=area)
            pk += 1
        fixture = fixture[:-1]
        data_file.write(fixture.encode('utf8'))
        data_file.write("\n]\n")


def cities_to_fixtures(cities):
    """Add cities fixtures file."""
    path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), '..', 'fixtures/initial_cities.json'))
    pk = 1
    fixture = ""
    with open(path, 'w') as data_file:
        data_file.write("[")
        for city in cities:
            # print city
            fixture += CITIES_FIXTURE.format(
                pk=pk, val=float(city[0]), area_name=city[1], name=city[2])
            pk += 1
        fixture = fixture[:-1]
        data_file.write(fixture.encode('utf8'))
        data_file.write("\n]\n")


def dcod_json_to_fixtures(path):
    """Prepare initial data."""
    data = dcod_parser(path)
    areas_to_fixtures(data[0])
    cities_to_fixtures(data[1])

if __name__ == "__main__":
    dir_path = os.path.dirname(os.path.realpath(__file__))
    dcod_json_to_fixtures(dir_path+"/2CuNPefD.json")
    # areas_to_fixtures(areas)
